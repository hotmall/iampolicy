package iampolicy

const (
	XAuthToken   = "X-Auth-Token"
	XUserId      = "X-User-Id"
	XUserName    = "X-User-Name"
	XProjectId   = "X-Project-Id"
	XProjectName = "X-Project-Name"
	XDomainId    = "X-Domain-Id"
	XDomainName  = "X-Domain-Name"
	XAppId       = "X-App-Id"
	XAppName     = "X-App-Name"
	XRoles       = "X-Roles"
	XUrl         = "X-Url"

	XAccessToken = "X-Access-Token"
	XCustomerId  = "X-Customer-Id"
	XUid         = "X-Uid"
)
