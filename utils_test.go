package iampolicy

import (
	"net/url"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestFormatUrl(t *testing.T) {
	assert := assert.New(t)
	cases := []struct {
		rawURL    string
		expectURL string
	}{
		{"/iam/v1/users", "/iam/v1/users"},
		{"/iam/v1/users?d=100&c=200&b=300&a=400", "/iam/v1/users?a=400&b=300&c=200&d=100"},
		{"/iam/v1/users?b=1,2,3,4,5&a=2,3,4,5", "/iam/v1/users?a=2,3,4,5&b=1,2,3,4,5"},
		{"/iam/v1/users?c=500&b=200&b=100&a=400&a=300", "/iam/v1/users?a=300&a=400&b=100&b=200&c=500"},
	}

	for _, item := range cases {
		u, err := url.Parse(item.rawURL)
		assert.Nil(err)
		r := formatUrl(u)
		assert.Equal(item.expectURL, r)
	}
}
