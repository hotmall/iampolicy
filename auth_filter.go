package iampolicy

import (
	"errors"
	"net/http"
	"strings"

	"github.com/emicklei/go-restful/v3"
	"gitlab.com/hotmall/keystoneclient"
	"gitlab.com/hotmall/keystoneclient/types"
	"gitlab.com/hotmall/portmatrix"
)

var (
	ErrNeedToken = errors.New("need a token")
	tokenAPI     keystoneclient.TokenAPI
)

type AuthFilter struct {
}

func NewAuthFilter() *AuthFilter {
	return &AuthFilter{}
}

func (f AuthFilter) Filter(req *restful.Request, resp *restful.Response, chain *restful.FilterChain) {
	token := req.HeaderParameter(XAuthToken)
	if token == "" {
		result := make(map[string]string, 1)
		result["error"] = ErrNeedToken.Error()
		resp.WriteHeaderAndEntity(http.StatusForbidden, result)
		return
	}

	ctx := types.NewGetTokenContext()
	ctx.XAuthToken = token
	ctx.XSubjectToken = token
	_, respBody, err := tokenAPI.GetToken(ctx)
	if err != nil {
		result := make(map[string]string, 1)
		result["error"] = err.Error()
		resp.WriteHeaderAndEntity(http.StatusForbidden, result)
		return
	}

	if respBody.Token.User != nil {
		req.Request.Header.Set(XUserId, respBody.Token.User.ID)
		req.Request.Header.Set(XUserName, respBody.Token.User.Name)
		if respBody.Token.User.Domain != nil {
			req.Request.Header.Set(XDomainId, respBody.Token.User.Domain.ID)
			req.Request.Header.Set(XDomainName, respBody.Token.User.Domain.Name)
		}
	}

	if respBody.Token.Domain != nil {
		req.Request.Header.Set(XDomainId, respBody.Token.Domain.ID)
		req.Request.Header.Set(XDomainName, respBody.Token.Domain.Name)
	}

	if respBody.Token.Project != nil {
		req.Request.Header.Set(XProjectId, respBody.Token.Project.ID)
		req.Request.Header.Set(XProjectName, respBody.Token.Project.Name)
		if respBody.Token.Project.Domain != nil {
			req.Request.Header.Set(XDomainId, respBody.Token.Project.Domain.ID)
			req.Request.Header.Set(XDomainName, respBody.Token.Project.Domain.Name)
		}
	}

	if len(respBody.Token.Methods) > 0 {
		method := respBody.Token.Methods[0]
		if method == "application_credential" {
			req.Request.Header.Set(XAppId, respBody.Token.ApplicationCredential.ID)
			req.Request.Header.Set(XAppName, respBody.Token.ApplicationCredential.Name)
		}
	}

	roles := make([]string, 0)
	for _, role := range respBody.Token.Roles {
		roles = append(roles, role.Name)
	}
	req.Request.Header.Set(XRoles, strings.Join(roles, ","))
	req.Request.Header.Set(XUrl, formatUrl(req.Request.URL))

	chain.ProcessFilter(req, resp)
}

func init() {
	tokenAPI = keystoneclient.NewTokenAPI(portmatrix.IDENTITY_HOST)
}
