package iampolicy

import (
	"net/url"
	"sort"
	"strings"
)

func IsAdmin(xroles string) bool {
	roles := strings.Split(xroles, ",")
	for _, role := range roles {
		if role == "admin" {
			return true
		}
	}
	return false
}

func formatUrl(url *url.URL) string {
	qs := strings.Split(url.RawQuery, "&")
	sort.Strings(qs)
	url.RawQuery = strings.Join(qs, "&")
	url.Path = strings.TrimRight(url.Path, "/")
	return url.String()
}
