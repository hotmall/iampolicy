package iampolicy

import (
	"net/http"

	"github.com/emicklei/go-restful/v3"
	"gitlab.com/hotmall/iamclient"
	iamtypes "gitlab.com/hotmall/iamclient/types"
	"gitlab.com/hotmall/portmatrix"
)

var (
	accessTokenAPI iamclient.AccessTokenAPI
)

type AccessFilter struct {
}

func NewAccessFilter() *AccessFilter {
	return &AccessFilter{}
}

func (f AccessFilter) Filter(req *restful.Request, resp *restful.Response, chain *restful.FilterChain) {
	token := req.HeaderParameter(XAccessToken)
	if token == "" {
		result := make(map[string]string, 1)
		result["error"] = ErrNeedToken.Error()
		resp.WriteHeaderAndEntity(http.StatusForbidden, result)
		return
	}
	ctx := &iamtypes.ShowAccessTokenContext{
		XAccessToken: token,
	}
	_, respBody, err := accessTokenAPI.ShowAccessToken(ctx)
	if err != nil {
		result := make(map[string]string, 1)
		result["error"] = err.Error()
		resp.WriteHeaderAndEntity(http.StatusForbidden, result)
		return
	}
	req.Request.Header.Set(XUid, respBody.UID)
	req.Request.Header.Set(XCustomerId, respBody.CustomerID)
	req.Request.Header.Set(XUrl, formatUrl(req.Request.URL))

	chain.ProcessFilter(req, resp)
}

func init() {
	accessTokenAPI = iamclient.NewAccessTokenAPI(portmatrix.IAM_HOST)
}
