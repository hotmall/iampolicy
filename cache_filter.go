package iampolicy

import (
	"context"
	"encoding/base64"
	"encoding/json"
	"errors"
	"time"

	"github.com/emicklei/go-restful/v3"
	"github.com/redis/go-redis/v9"
)

var (
	errBytesIsNil = errors.New("bytes is nil")
)

type CacheFilter struct {
	redisClient redis.UniversalClient
}

func NewCacheFilter(client redis.UniversalClient) *CacheFilter {
	return &CacheFilter{
		redisClient: client,
	}
}

func (f CacheFilter) GetCache(key string) (value []byte, err error) {
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()
	v, err := f.redisClient.Get(ctx, key).Result()
	if err != nil {
		return
	}
	value, err = base64.StdEncoding.DecodeString(v)
	return
}

// SetCache: 设置缓存，默认过期时间 24 小时
func (f CacheFilter) SetCache(key string, v interface{}) (err error) {
	err = f.SetCacheExp(key, v, 24*time.Hour)
	return
}

func (f CacheFilter) SetCacheExp(key string, v interface{}, expiration time.Duration) (err error) {
	bs, err := json.Marshal(v)
	if err != nil {
		return
	}
	value := base64.StdEncoding.EncodeToString(bs)
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()
	_, err = f.redisClient.Set(ctx, key, value, expiration).Result()
	return
}

func (f CacheFilter) SetBytesExp(key string, bs []byte, expiration time.Duration) (err error) {
	if bs == nil {
		return errBytesIsNil
	}
	value := base64.StdEncoding.EncodeToString(bs)
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()
	_, err = f.redisClient.Set(ctx, key, value, expiration).Result()
	return
}

func (f CacheFilter) DelCache(keys ...string) (err error) {
	if len(keys) > 0 {
		ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
		defer cancel()
		_, err = f.redisClient.Del(ctx, keys...).Result()
	}
	return
}

// 根据模式 pattern 删除 cache
func (f CacheFilter) DelCacheByPattern(pattern string) (err error) {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	keys, err := f.redisClient.Keys(ctx, pattern).Result()
	if err != nil {
		return
	}
	if len(keys) > 0 {
		_, err = f.redisClient.Del(ctx, keys...).Result()
	}
	return
}

func (f CacheFilter) Filter(req *restful.Request, resp *restful.Response, chain *restful.FilterChain) {
	xurl := req.Request.Header.Get(XUrl)
	if xurl == "" {
		xurl = formatUrl(req.Request.URL)
		req.Request.Header.Set(XUrl, xurl)
	}

	if req.Request.Method == "GET" {
		// GET 操作都设置下缓存, 目前缓存超时时间 24 小时
		if value, err := f.GetCache(xurl); err == nil {
			// 在 cache 存在，直接将结果返回
			resp.Header().Set(restful.HEADER_ContentType, restful.MIME_JSON)
			resp.Write(value)
			return
		}
	}
	chain.ProcessFilter(req, resp)
}
