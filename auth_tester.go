package iampolicy

import "github.com/emicklei/go-restful/v3"

type AuthTester struct {
}

func NewAuthTester() *AuthTester {
	return &AuthTester{}
}

func (f AuthTester) Filter(req *restful.Request, resp *restful.Response, chain *restful.FilterChain) {
	req.Request.Header.Set(XAuthToken, "gAAAAABgs2SfrHEKr_KcwI2Z0uqmYjKzZngZkF3jWzgs9E2X0meI8Y1fpBK2Hedok-JMflINpRoalZmGxDD9cSCRsFFffhdUbKvwpveBQZebUuzHTUGHDKcr8UVrZmnynsGe0AF06T3AT3urEi_ECSsCXlnQZbV7tytzd65_HvL8UYj7KBY_S7o")
	req.Request.Header.Set(XUserId, "2844b2a08be147a08ef58317d6471f1f")
	req.Request.Header.Set(XProjectId, "0c4e939acacf4376bdcd1129f1a054ad")
	req.Request.Header.Set(XDomainId, "5a75994a383c449184053ff7270c4e91")
	req.Request.Header.Set(XRoles, "admin,sp_adm,sp_mgr,sp_clk,ml_adm,ml_mgr,ml_clk,mg_adm,mg_mgr,mg_clk,br_adm,br_mgr,br_clk,op_adm,op_mgr,op_clk,om_adm,om_mgr,om_clk")
	req.Request.Header.Set(XUrl, formatUrl(req.Request.URL))
	chain.ProcessFilter(req, resp)
}
