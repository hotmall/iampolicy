package iampolicy

import "github.com/emicklei/go-restful/v3"

type AccessTester struct {
}

func NewAccessTester() *AccessTester {
	return &AccessTester{}
}

func (f AccessTester) Filter(req *restful.Request, resp *restful.Response, chain *restful.FilterChain) {
	req.Request.Header.Set(XAccessToken, "gAAAAABgs2SfrHEKr_KcwI2Z0uqmYjKzZngZkF3jWzgs9E2X0meI8Y1fpBK2Hedok-JMflINpRoalZmGxDD9cSCRsFFffhdUbKvwpveBQZebUuzHTUGHDKcr8UVrZmnynsGe0AF06T3AT3urEi_ECSsCXlnQZbV7tytzd65_HvL8UYj7KBY_S7o")
	req.Request.Header.Set(XCustomerId, "184582449563439104")
	req.Request.Header.Set(XUrl, formatUrl(req.Request.URL))
	chain.ProcessFilter(req, resp)
}
