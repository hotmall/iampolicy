module gitlab.com/hotmall/iampolicy

go 1.21

require (
	github.com/emicklei/go-restful/v3 v3.11.1
	github.com/redis/go-redis/v9 v9.4.0
	github.com/stretchr/testify v1.8.4
	gitlab.com/hotmall/iamclient v0.0.0-20240308033216-61a51f664499
	gitlab.com/hotmall/keystoneclient v0.0.0-20240108071144-0fa96c2a5600
	gitlab.com/hotmall/portmatrix v1.0.0
)

require (
	github.com/andybalholm/brotli v1.1.0 // indirect
	github.com/cespare/xxhash/v2 v2.2.0 // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/dgryski/go-rendezvous v0.0.0-20200823014737-9f7001d12a5f // indirect
	github.com/hotmall/requests v1.2.0 // indirect
	github.com/klauspost/compress v1.17.7 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/valyala/bytebufferpool v1.0.0 // indirect
	github.com/valyala/fasthttp v1.52.0 // indirect
	gopkg.in/validator.v2 v2.0.1 // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
